# Getting Started with ml5.js

## Objective

Learn how to use [ml5.js](https://ml5js.org/).

## Motivation

I saw Daniel Shiffman from The Coding Train using it and thought it looked cool, so I tried it for myself and found it to be pretty cool.

## Credits

[Getting Started &centerdot; ml5js](https://ml5js.org/docs/getting-started.html)